#!/bin/sh

port=443
starttls=""

if [ -z "${1}" -o -n "${4}" ]; then
    echo "Usage: host [port] [starttls-protocol]" 1>&2
    exit 1;
fi

if [ -n "${2}" ]; then
    port="${2}"
fi

if [ -n "${3}" ]; then
    starttls="-starttls ${3}"
fi

echo QUIT | \
    openssl s_client -connect ${1}:${port} ${starttls} 2>&1 | \
    openssl x509 -in /dev/stdin -noout -text 2>&1 | \
    egrep -A2 'Validity|X509v3 Subject Alternative Name|Issuer' | \
    egrep -v '^--|X509v3 Certificate Policies'

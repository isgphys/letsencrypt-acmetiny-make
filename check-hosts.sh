#!/bin/bash

num=1
failed=0
random="$(head -c32 < <(LC_CTYPE=C tr -dc 'a-zA-Z0-9' < /dev/urandom))"
acme_dir=/var/www/html/.well-known/acme-challenge

function cleanup {
    if [ -f "${acme_dir}/${random}" ]; then
        rm "${acme_dir}/${random}"
    fi
}

function abort {
    >&2 echo "Error: local well-known/acme-challenge check aborted unexpectedly"
    cleanup
    exit 1
}

set -e
trap abort ERR

mkdir -p ${acme_dir}
chown root:letsencrypt ${acme_dir}
chmod 0775 ${acme_dir}
su - letsencrypt -s /bin/bash -c "echo ${random} > ${acme_dir}/${random}"

function check {
  local NO="\e[0m"  # NORMAL FONT
  local RE="\e[31m" # RED
  local GR="\e[32m" # GREEN
  local YE="\e[33m" # YELLOW
  local BL="\e[34m" # BLUE

  dns="${1}"
  ret="$(GET http://${dns}/.well-known/acme-challenge/${random} || true)"
  if [ "${ret}" == "${random}" ]; then
    printf "checking %03d ${GR}success${NO} ${dns}\n" ${num}
  else
    >&2 printf "checking %03d ${RE}failed${NO} ${dns}\n" ${num}
    failed=1
  fi
  ((num++))
}

function check_file {
  echo checking san file "${1}"
  while read line
  do
    check "${line}"
  done < "${1}"
}

while [[ ${#} -gt 0 ]]; do
  key="${1}"
  case ${key} in
    -f|--file)
      check_file "${2}"
      shift
      ;;
    *)
      check "${1}"
      ;;
  esac
  shift
done

set +e

cleanup

if [ "${failed}" == 1 ]; then
  exit 1
else
  exit 0
fi

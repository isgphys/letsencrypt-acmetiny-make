#!/bin/bash
# This script renews the letsencrypt certificate.

LOG=log/renew.log
PATH="/usr/sbin:/usr/bin:/sbin:/bin:${PATH}"
export PATH

function fail {
    >&2 echo "${1}"  # log to stderr
    {
        echo "Check the log at: ${PWD}/${LOG}"
        echo "Last 50 lines of log:"
        tail -n50 "${LOG}"
    } >&4  # send output of block to orignal stderr (without logfile)
    exit 1
}

function abort {
    fail "Error: Letsencrypt renewal failed with non-zero exit code (trap abort ERR)"
}

function retry {
    local n=1
    local max=7
    local delay=300
    while true; do
        "$@" 2>&1 && break || {
            if [[ ${n} -lt ${max} ]]; then
                backoff=$(( n*delay ))
                echo "renewal attempt ${n} failed. retrying in ${backoff} seconds:"
                ((n++))
                sleep ${backoff};
            else
              fail "Error: Letsencrypt renewal failed after ${n} attempts"
            fi
        }
    done
}

set -e
trap abort ERR

cd /opt/letsencrypt
mkdir -p log

# Save original file descriptors in 3 and 4 for later reuse
# Redirect (append) STDOUT to LOG and STDERR to FIFO >(...)
# >(...) (process substitution) creates a FIFO and lets tee listen on it
# tee reads from STDIN, appends to LOG, redirect STDOUT to STDERR again
exec 3>&1 4>&2 1>> "${LOG}" 2> >(tee -a "${LOG}" >&2)

echo "renewing certificate $(date -Iseconds)"
touch --no-create account.key
retry make
set +e

# Reload services as needed.

for service in apache2 nginx postfix dovecot; do
    if systemctl is-enabled --quiet "${service}.service" >/dev/null 2>&1; then
        systemctl reload "${service}.service"
    fi
done

if systemctl is-enabled --quiet mariadb.service >/dev/null 2>&1; then
    MARIADB_ENABLED="$(mariadb -N -sne 'SELECT @@GLOBAL.have_ssl;')"
    if [ "${MARIADB_ENABLED}" = "YES" ]; then
        mariadb-admin flush-ssl
    fi
fi

if [ -f renew.local.sh ]; then
    source renew.local.sh
fi

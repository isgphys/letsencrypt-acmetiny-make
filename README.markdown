Let's Encrypt with acme-tiny
============================

Get and renew Let's Encrypt TLS certificates based on [acme-tiny](https://github.com/diafygi/acme-tiny)


Notes
-----

- Uses [ACMEv2 (RFC 8555) via local](https://letsencrypt.org/2019/03/11/acme-protocol-ietf-standard.html) `acme_tiny.py` v5.0.1
- [End of Life Plan for ACMEv1](https://community.letsencrypt.org/t/end-of-life-plan-for-acmev1/88430)
- ACME v2 - [Scheduled deprecation of unauthenticated resource GETs](https://community.letsencrypt.org/t/acme-v2-scheduled-deprecation-of-unauthenticated-resource-gets/74380):


How it works
------------

The configuration is read from the files in `hosts/`, where one or more file(s) contain the list of all vhosts and aliases. Each file will result in a separate certificate. Due to the currently maximum allowed number of 100 subjectAltNames per certificate multiple certificates might be needed for a host.

The basic idea is, that you type `make` and it builds a new CSR, a new key and gets a new TLS certificate, stored in this directory, from where it can be integrated in other services' configurations. If no certificate could be retrieved, key and certificate won't get overwritten.

After the initial make, that generates the private keys, only the certificates will be renewed. Should you want to rotate keys, do it manually by removing the respective `.key`, `.csr` and `.crt` files.

Running `make` or `make all` will generate both **RSA** and **ECDSA** key-/certificate-pairs.

A cronjob can be installed to run every month at a random time during office hours.


Requirements
------------

The verification is done by `acme-tiny` placing requested files into `/var/www/html/.well-known/acme-challenge`, hence that directory needs to be accessible from all VHosts as `http://$VHOST/.well-known/acme-challenge`.

A webserver must be running on port `80` and configured to serve requests for the acme-challenge files from this URL. See below for some examples.


Setup
-----

* Install the depenencies:

```sh
apt-get install git ssl-cert make libwww-perl
```

* Clone this repository into `/opt/letsencrypt`, then:

```sh
cd /opt/letsencrypt
```

* Create a system user `letsencrypt` with group `letsencrypt` and `/opt/letsencrypt` as home directory:

```sh
make user
make permissions
```

* Create a hosts file containing subjectAltNames. Example:

```sh
mkdir -p hosts
echo "$(hostname -f)" >> hosts/le0
echo "www.$(hostname -f)" >> hosts/le0
```

* Optional: Check that URI `/.well-known/acme-challenge/` is accessible on all domains (automatically on `make`):

```sh
make check
```

* Generate the certificate(s):

```sh
make
```

* If it worked, install the cron job:

```sh
make cron
```

* Optional: To check when it is run:

```sh
make show-cron
```


Use staging environment
----------------------

Let's encrypt has a [staging environment](https://letsencrypt.org/docs/staging-environment/) which has relaxed rate-limiting but no trusted root cert.
Enable staging by adding `ACME_DIRECTORY_URL=https://acme-staging-v02.api.letsencrypt.org/directory` to `acme.conf`:

```sh
# cat acme.conf
ACME_DIRECTORY_URL=https://acme-staging-v02.api.letsencrypt.org/directory
```
This URL will be passed to `acme_tiny.py` as `--directory-url`.

Monitoring
----------

* Make sure at least one vhost with HTTPS is monitored, to get an alert if the certificate was not properly renewed.


Overview of the files
---------------------

* `Makefile`: The main magic is in here.
* `account.key`: Key which represents the account with which the certificates are associated. `acme-tiny` currently doesn't support to associate an e-mail address with this key/account.
* [Intermediate (Chain) certificates](https://letsencrypt.org/certificates/).
* `hosts/`: Directory with one config file per certificate (possibly multiple certificates per host).
* `log/`: Directory for renew log files.
* `archive/certs/`: Directory contains all old certificates
* `renew.sh`: The script performing the certificate renewal.
* `renew.local.sh`: Optional script for reload of certificates in additional services.
* `check-remote-certificate.sh`: Helper tool to identify which TLS certificate is running on a TLS server.
* `check-hosts.sh`: Checker script invoked when running `make check` or `make check-<le[#]>`
* `acme_tiny.py`: The acme-tiny script
* `acme.conf` (optional): extra options for `acme_tiny.py`

Overview of `make` targets
--------------------------

* `make`: makes all certificates `hosts/*`
* `make rsa|ecdsa`: makes just the specified cert type
* `make <cert>.crt`: makes just the given certificate
* `make check`: makes a local `/.well-known/` check for all certificates
* `make check-<cert>`: makes a /.well-known check hust for the given certificate
* `make cron`: makes the cron job for the host
* `make show-cron`: shows the cron job of the host
* `make user`: creates the letsencrypt system user account
* `make clean`: removes any `*.tmp` files from failed makes
* `make clean-cron`: removes the cron job
* `make clean-user`: removes the letsencrypt user


Webserver example configs
-------------------------

### Apache

```
<IfModule proxy_module>
  ProxyPass /.well-known/acme-challenge !
</IfModule>

Define well_known_root "/var/www/html"

Alias /.well-known/acme-challenge/ "${well_known_root}/.well-known/acme-challenge/"

<Directory "${well_known_root}/.well-known/acme-challenge">
  Options None
  AllowOverride None
  Require all granted
</Directory>

<Location /.well-known/acme-challenge>
  Options None
  Require all granted
</Location>
```

### Nginx

```
location /.well-known/acme-challenge/ {
    root /var/www/html;
    allow all;
    autoindex off;
    try_files $uri $uri/ =404;
}
```

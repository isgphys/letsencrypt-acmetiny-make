-include acme.conf
CERTNAMES := $(sort $(notdir $(wildcard hosts/*)))
CERTFILES_RSA := $(addsuffix .rsa.crt, $(CERTNAMES))
CERTFILES_ECDSA := $(addsuffix .ecdsa.crt, $(CERTNAMES))
CERTCHECKS := $(addprefix check-, $(CERTNAMES))
MKFILE_PATH := $(abspath $(lastword $(MAKEFILE_LIST)))
MKFILE_DIR := $(patsubst %/,%,$(dir $(MKFILE_PATH)))
CHECK := $(MKFILE_DIR)/check-hosts.sh
ACME := $(MKFILE_DIR)/acme_tiny.py
ACME_DIR := /var/www/html/.well-known/acme-challenge
ACME_EXTRA_OPTS := $(shell [ ! -z ${ACME_DIRECTORY_URL} ] && echo "--directory-url ${ACME_DIRECTORY_URL}")
CRONJOB := /etc/cron.d/letsencrypt-acmetiny
RENEW := $(MKFILE_DIR)/renew.sh

all: ecdsa rsa

ecdsa: $(CERTFILES_ECDSA)

rsa: $(CERTFILES_RSA)

check: $(CERTCHECKS)

check-%:
	$(CHECK) -f hosts/$*

clean:
	rm -f *.tmp

%.cfg: hosts/% default.cfg
	cat default.cfg > $*.cfg
	printf "\n[ req_ext ]\n" >> $*.cfg
	printf "subjectAltName = %s\n" `cat hosts/$* | xargs -n1 printf "DNS:%s," | sed -e 's/,$$//'` >> $*.cfg

%.rsa.crt: %.rsa.csr account.key $(wildcard acme.conf)
	$(MAKE) check-$*
	mkdir -pv $(ACME_DIR)
	chown -c root:letsencrypt $(ACME_DIR)
	chmod -c 0775 $(ACME_DIR)
	touch $@.tmp
	chmod -c 0664 $@.tmp
	chown -c root:letsencrypt $@.tmp
	su - letsencrypt -s /bin/sh -c "$(ACME) ${ACME_EXTRA_OPTS} --quiet --account-key account.key --csr $< --acme-dir $(ACME_DIR) > $@.tmp"
	mkdir -pv archive/cert
	test -s $@.tmp && fgrep -q -- '-----END CERTIFICATE-----' $@.tmp && \
	test -e $@ && mv -v $@ archive/cert/$(shell date -Iseconds)_$@ || true && \
	mv -v $@.tmp $@
	$(MAKE) permissions

%.rsa.csr: %.cfg %.rsa.key
	openssl req -new -config $< -key $*.rsa.key -out $@ -subj "/"
	openssl req -in $@ -noout -text -nameopt sep_multiline

%.rsa.key:
	openssl genrsa -out $@ 4096 2>&1

%.ecdsa.crt: %.ecdsa.csr account.key $(wildcard acme.conf)
	$(MAKE) check-$*
	mkdir -pv $(ACME_DIR)
	chown -c root:letsencrypt $(ACME_DIR)
	chmod -c 0775 $(ACME_DIR)
	touch $@.tmp
	chmod -c 0664 $@.tmp
	chown -c root:letsencrypt $@.tmp
	su - letsencrypt -s /bin/sh -c "$(ACME) ${ACME_EXTRA_OPTS} --quiet --account-key account.key --csr $< --acme-dir $(ACME_DIR) > $@.tmp"
	mkdir -pv archive/cert
	test -s $@.tmp && fgrep -q -- '-----END CERTIFICATE-----' $@.tmp && \
	test -e $@ && mv -v $@ archive/cert/$(shell date -Iseconds)_$@ || true && \
	mv -v $@.tmp $@
	$(MAKE) permissions

%.ecdsa.csr: %.cfg %.ecdsa.key
	openssl req -new -config $< -key $*.ecdsa.key -out $@ -subj "/"
	openssl req -in $@ -noout -text -nameopt sep_multiline

%.ecdsa.key:
	openssl ecparam -genkey -name secp384r1 -out $@

account.key:
	openssl genpkey -algorithm rsa -pkeyopt rsa_keygen_bits:4096 -outform PEM -out $@
	$(MAKE) permissions

permissions:
	chmod -c 0640 account.key || true
	chown -c root:letsencrypt account.key || true
	chmod -cf 0640 *.rsa.key *.ecdsa.key || true
	chown -cf root:ssl-cert *.rsa.key *.ecdsa.key || true
	chmod -c 0644 *.crt || true
	chown -c root:root *.crt || true
	chmod -c 0711 .
	chown -c root:ssl-cert .

cron-job-time:
	$(eval day := $(shell shuf -i 1-27 -n 1))
	$(eval hour := $(shell shuf -i 9-15 -n 1))
	$(eval min := $(shell shuf -i 0-59 -n 1))

cron: cron-job-time $(CRONJOB)

$(CRONJOB):
	printf "%2s %2s %2s  *  *  root  $(RENEW)\n" ${min} ${hour} ${day} > $(CRONJOB)
	chmod 644 $(CRONJOB)

clean-cron:
	if [ -e $(CRONJOB) ]; then rm $(CRONJOB); fi

show-cron-header:
	@echo " +-------------------------- min   (0 - 59)"
	@echo " |  +----------------------- hour  (9 - 15)"
	@echo " |  |  +-------------------- day   (1 - 27)"
	@echo " |  |  |  +----------------- month (each)"
	@echo " |  |  |  |  +-------------- year  (each)"
	@echo " |  |  |  |  |   +---------- run as"
	@echo " |  |  |  |  |   |     +---- cmd"
	@echo " |  |  |  |  |   |     |"

show-cron: show-cron-header
	@cat $(CRONJOB) || true

user:
	groupadd -r ssl-cert || true
	groupadd -r letsencrypt || true
	useradd -d $(MKFILE_DIR) -s /usr/sbin/nologin -r -g letsencrypt letsencrypt || true
	usermod -d $(MKFILE_DIR) -s /usr/sbin/nologin -L -g letsencrypt letsencrypt || true

clean-user:
	userdel letsencrypt || true
	groupdel letsencrypt || true

check-sys:
	@apache2ctl -t; true
	@service apache2 status; true
	@echo
	@dpkg -l apache2 | grep apache2 ; true
	@echo
	@find /etc/apache2/ -name "*well-known*" -print | xargs ls -l; true
	@echo
	@find /etc/cron.d/ -name "*letsencrypt*" -print | xargs ls -l; true
	@echo
	@$(MAKE) show-cron
	@echo
	@find . -name "*.crt" -print | xargs -I % openssl x509 -text -in % | grep -E '(Subject: CN|Not After :|DNS:)' | sed 's/^[ \t]*//'

.SECONDARY: account.key

# Mark intermediate files as precious to prevent automatic deletion by make
.PRECIOUS: %.cfg %.csr %.rsa.csr %.ecdsa.csr %.key %.rsa.key %.ecdsa.key
.PHONY: clean check check-%
